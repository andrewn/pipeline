package org.moodle.ci.versions;

class Master extends Version {
    Master() {
        this.name = "master"
        this.defaultBranch = "master"
        this.supportedPHPVersions = [
            "7.0",
            "7.1",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
