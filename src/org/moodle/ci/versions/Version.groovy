package org.moodle.ci.versions;

class Version {
    protected String name

    protected String defaultBranch

    protected String[] supportedPHPVersions

    protected String[] supportedDatabases

    def getDefaultBranch() {
        return defaultBranch
    }

    def getPhpVersions() {
        return supportedPHPVersions
    }

    def getDatabases() {
        return supportedPHPVersions
    }

    def getHighestSupportedVersion() {
        def versions = getPhpVersions()

        return versions[versions.size() - 1]
    }

    def getLowestSupportedVersion(branch) {
        def versions = getPhpVersions()

        return versions[0]
    }
}
