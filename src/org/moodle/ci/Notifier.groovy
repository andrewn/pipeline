/**
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.moodle.ci

import com.tikal.hudson.plugins.notification.*
import com.tikal.hudson.plugins.notification.model.BuildState
import com.tikal.hudson.plugins.notification.model.JobState
import com.tikal.hudson.plugins.notification.model.ScmState
import hudson.EnvVars
import hudson.model.*
import jenkins.model.Jenkins

class Notifier {
    public static void notify(Task task, steps) {
        int triesRemaining = 3
        boolean failed = false
        do {
            // Represents a string that will be put into the log
            // if there is an error contacting the target.
            String urlIdString = "url 'unknown'"
            try {
                EnvVars environment = run.getEnvironment(listener)
                // Expand out the URL from environment + url.
                String expandedUrl = "https://telegrambot.moodle.org/hubot/jenkinsnotify"
                expandedUrl = "http://boysenberry.local/foo/test.php"
                urlIdString = String.format("url '%s'", expandedUrl)
                listener.getLogger().println(String.format("Notifying endpoint with %s", urlIdString))
                JobState jobState = buildJobState(run.getParent(), run, listener, timestamp)
                Protocol.HTTP.send(expandedUrl,
                                   Format.JSON.serialize(jobState),
                                   10000,
                                   true)
            } catch (Throwable error) {
                failed = true
                error.printStackTrace( listener.error( String.format( "Failed to notify endpoint with %s", urlIdString)))
                listener.getLogger().println( String.format( "Failed to notify endpoint with %s - %s: %s",
                                                              urlIdString, error.getClass().getName(), error.getMessage()))
                if (triesRemaining > 0) {
                    listener.getLogger().println( String.format( "Reattempting to notify endpoint with %s (%d tries remaining)", urlIdString, triesRemaining))
                }
            }
        }
        while (failed && --triesRemaining >= 0)
    }

    private static JobState buildJobState(Job job, Run run, TaskListener listener, long timestamp)
        throws IOException, InterruptedException
    {

        Jenkins            jenkins      = Jenkins.getInstance()
        String             rootUrl      = jenkins.getRootUrl()
        JobState           jobState     = new JobState()
        BuildState         buildState   = new BuildState()
        ScmState           scmState     = new ScmState()
        Result             result       = run.getResult()
        ParametersAction   paramsAction = run.getAction(ParametersAction.class)
        EnvVars            environment  = run.getEnvironment( listener )
        StringBuilder      log          = getLog(run)

        jobState.setName( job.getName())
        jobState.setDisplayName(job.getDisplayName())
        jobState.setUrl( job.getUrl())
        jobState.setBuild( buildState )

        buildState.setNumber( run.number )
        buildState.setQueueId( run.getQueueId() )
        buildState.setUrl( run.getUrl())
        buildState.setPhase( this )
        buildState.setTimestamp( timestamp )
        buildState.setScm( scmState )
        buildState.setLog( log )

        if ( result != null ) {
            buildState.setStatus(result.toString())
        }

        if ( rootUrl != null ) {
            buildState.setFullUrl(rootUrl + run.getUrl())
        }

        buildState.updateArtifacts( job, run )

        if ( paramsAction != null ) {
            EnvVars env = new EnvVars()
            for (ParameterValue value : paramsAction.getParameters()){
                if ( ! value.isSensitive()) {
                    value.buildEnvironment( run, env )
                }
            }
            buildState.setParameters(env)
        }

        if ( environment.get( "GIT_URL" ) != null ) {
            scmState.setUrl( environment.get( "GIT_URL" ))
        }

        if ( environment.get( "GIT_BRANCH" ) != null ) {
            scmState.setBranch( environment.get( "GIT_BRANCH" ))
        }

        if ( environment.get( "GIT_COMMIT" ) != null ) {
            scmState.setCommit( environment.get( "GIT_COMMIT" ))
        }

        return jobState
    }

    private StringBuilder getLog(Run run) {
        StringBuilder log = new StringBuilder("")
        // TODO paramaterise this.
        Integer loglines = 10

        if (loglines == null || loglines == 0) {
            return log
        }

        try {
            switch (loglines) {
                // The full log
                case -1:
                    log.append(run.getLog())
                    break
                default:
                    List<String> logEntries = run.getLog(loglines)
                    for (String entry: logEntries) {
                        log.append(entry)
                        log.append("\n")
                    }
            }
        } catch (IOException e) {
            log.append("Unable to retrieve log")
        }
        return log
    }
}
