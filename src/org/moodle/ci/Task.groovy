package org.moodle.ci;

import org.moodle.ci.repositories.Repository
import org.moodle.ci.versions.Version

class Task {
    protected Repository repository

    protected Version moodleVersion

    protected String url

    protected String credentialsId

    protected String branch

    protected String database = 'pgsql'

    protected String phpVersion

    protected String task = 'phpunit'

    protected Boolean notify = false

    protected String pathToRunner

    def getUrl() {
        if (url) {
          return url
        } else if (repository) {
          return repository.url
        }

        throw new Exception('No repository specified.')
    }

    def getCredentialsId() {
        if (credentialsId) {
          return credentialsId
        } else if (repository && repository.credentialsId) {
          return repository.credentialsId
        }

        return null
    }

    def getBranch() {
        if (branch) {
          return branch
        } else if (moodleVersion) {
          return moodleVersion.defaultBranch
        }

        throw new Exception('No Pull URL set')
    }

    def getPhpVersion() {
        if (phpVersion) {
          return phpVersion
        } else if (moodleVersion) {
          return moodleVersion.getHighestSupportedVersion()
        }

        throw new Exception("I don't know what PHP Version to test against")
    }

    def getPathToRunner(env, steps) {
        def runner
        if (pathToRunner) {
            runner = new File(pathToRunner)
            if (!runner.exists()) {
                throw new Exception("Unable to find job runner at ${pathToRunner}")
            }

            return runner.getAbsolutePath()
        } else if (moodleVersion) {
            runner = new File("${env.HOME}/scripts/runner/${moodleVersion.name}/run.sh")
            if (!runner.exists()) {
                runner = new File("${env.HOME}/scripts/runner/master/run.sh")
            }

            return runner.getAbsolutePath()
        } else {
            def versionName = steps.sh (
              script: 'grep "\\$branch" moodle/version.php | sed "s/^[^\']*\'\\([^\']*\\).*\$/\\1/"',
              returnStdout: true
              ).trim()
            runner = new File("${env.HOME}/scripts/runner/${versionName}/run.sh")

            if (!runner.exists()) {
              runner = new File("${env.HOME}/scripts/runner/master/run.sh")
            }

            return runner.getAbsolutePath()
        }

        throw new Exception("Unable to determine path to script runner.")
    }
}
