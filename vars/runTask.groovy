def call(org.moodle.ci.Task task) {
    pipeline {
        agent {
            label 'docker'
        }

        environment {
            PHP_VERSION = "${task.phpVersion}"
            DBTYPE = "${task.database}"
            TESTTORUN = "${task.task}"
            PATHTORUNNER = "${HOME}/scripts/runner"
        }

        stages {
            stage("Git Checkout") {
                steps {
                    checkout(
                        [
                            $class: 'GitSCM',
                            branches: [
                                [
                                    name: "${task.branch}"
                                ]
                            ],
                            doGenerateSubmoduleConfigurations: false,
                            extensions: [
                                [
                                    $class: 'CloneOption',
                                    depth: 0,
                                    noTags: false,
                                    reference: "${env.HOME}/cache/integration.git",
                                    shallow: true
                                ],
                                [
                                    $class: 'RelativeTargetDirectory',
                                    relativeTargetDir: 'moodle'
                                ]
                            ],
                            submoduleCfg: [],
                            userRemoteConfigs: [
                                [
                                    url: "${task.url}",
                                    credentialsId: "${task.credentialsId}"
                                ]
                            ]
                        ]
                    )
                }
            }

            stage("Run Task") {
                steps {
                    wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                        sh task.getPathToRunner(env, steps)
                    }
                }
            }

        }
        post {
            always {
                junit allowEmptyResults: true, testResults: "${env.BUILD_ID}/*.junit/*.xml,${env.BUILD_ID}/*.junit"
                archiveArtifacts allowEmptyArchive: true, artifacts: "${env.BUILD_ID}/**"
                cleanWs deleteDirs: true, notFailBuild: true

                // TODO Make use of the notify.
            }
        }
    }
}

return this
